<div id="news-pages-pager">
  <?php foreach ($news as $article): ?>
    <div>
      <h2><?php echo $article->subtitle['und'][0]['value'] ?></h2>
      <h4><?php echo $article->description['und'][0]['value'] ?></h4>
    </div>
  <?php endforeach; ?>
</div>