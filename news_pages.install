<?php

/**
 * Implements hook_install().
 */
function news_pages_install() {
  node_types_rebuild();
  $types = node_type_get_types();
  node_add_body_field($types['news_pages']);

  //New way to implement to add fields in your content type
  foreach (_news_pages_installed_fields() as $field) {
    field_create_field($field);
  }
  foreach (_news_pages_installed_instances() as $fieldinstance) {
    $fieldinstance['entity_type'] = 'node';
    $fieldinstance['bundle'] = 'news_pages';
    field_create_instance($fieldinstance);
  }
}

/**
 * @return array
 */
function _news_pages_installed_instances() {
  $t = get_t();
  return [
    'subtitle' => [
      'field_name' => 'subtitle',
      'type' => 'text',
      'label' => $t('Subtitle'),
      'widget' => [
        'type' => 'text_textfield',
      ],
      'display' => [
        'example_node_list' => [
          'label' => $t('Subtitle'),
          'type' => 'text',
        ],
      ],
    ],
    'description' => [
      'field_name' => 'description',
      'type' => 'text',
      'label' => $t('Description'),
      'widget' => [
        'type' => 'text_textarea_with_summary',
      ],
      'display' => [
        'example_node_list' => [
          'label' => $t('Description'),
          'type' => 'text',
        ],
      ],
    ],
  ];
}

/**
 * @return array
 */
function _news_pages_installed_fields() {
  $t = get_t();
  return [
    'subtitle' => [
      'field_name' => 'subtitle',
      'label' => $t('Subtitle'),
      'type' => 'text',
    ],
    'description' => [
      'field_name' => 'description',
      'label' => $t('Description'),
      'type' => 'text',
    ],
  ];
}

function news_pages_uninstall() {
  // machine name of the content type
  $name = 'news_pages';

  // gather all job nodes created
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, [':type' => $name]);
  $nids = [];
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }
  // find all fields and delete them
  foreach (array_keys(_news_pages_installed_fields()) as $field) {
    field_delete_field($field);
  }

  // find all fields and delete instance
  $instances = field_info_instances('node', $name);
  foreach ($instances as $instance_name => $instance) {
    field_delete_instance($instance);
  }

  node_delete_multiple($nids);
  // delete our content type
  node_type_delete($name);

  // purge all field infromation
  field_purge_batch(1000);
}